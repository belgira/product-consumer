package com.intevalue.producer;

import com.intevalue.util.Buffer;

public class Producer implements Runnable {

	private Buffer buffer;
	private int capacity;
	
	public Producer(Buffer buffer, int capacity) {
		this.buffer = buffer;
		this.capacity = capacity;
	}

	@Override
	public void run() {
		try {
			for (Integer i = 1; i < capacity + 1; ++i) {
				System.out.println("Produce product");
				Thread.sleep(100);
				buffer.put(i);
			}
			buffer.setContinue(false);
		} catch (InterruptedException i) {
			i.printStackTrace();
		}
	}

}
