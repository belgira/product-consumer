package com.intevalue.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Buffer {
	private boolean isContinue = true;
	LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<>();

	public void put(Integer data) throws InterruptedException {
		this.queue.put(data);
	}

	public Integer get() throws InterruptedException {
		return this.queue.poll(1, TimeUnit.SECONDS);
	}

	public boolean isContinue() {
		return isContinue;
	}

	public void setContinue(boolean isContinue) {
		this.isContinue = isContinue;
	}

}