package com.intevalue.consumer;

import com.intevalue.util.Buffer;

public class Consumer implements Runnable {

	private Buffer buffer;

	public Consumer(Buffer buffer) {
		this.buffer = buffer;
	}

	@Override
	public void run() {
		try {
			Integer id = buffer.get();
			while (buffer.isContinue() || id != null) {
				Thread.sleep(1000);
				System.out.println("Consume product");
				id = buffer.get();
			}
		} catch (InterruptedException i) {
			i.printStackTrace();
		}
	}

}
