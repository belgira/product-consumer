package com.intevalue;

import com.intevalue.concurrency.ThreadRunner;

public class Application {
	public static void main(String args[]) {
		ThreadRunner threadRunner = new ThreadRunner();
		// iteration less than or equal to zero is infinite run
		int iteration = -1;
		int capacity = 1;
		threadRunner.runProcess(iteration, capacity);
	}
}
