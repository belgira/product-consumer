package com.intevalue.concurrency;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.intevalue.consumer.Consumer;
import com.intevalue.producer.Producer;
import com.intevalue.util.Buffer;

public class ThreadRunner {

	public void runProcess(int iteration, int capacity) {
		ExecutorService executorService = Executors.newCachedThreadPool();
		if (isLimited(iteration)) {
			runLimited(executorService, iteration, capacity);
		} else {
			runInfinite(executorService, capacity);
		}

	}

	private void runProcess(ExecutorService executorService, int capacity)
			throws InterruptedException, ExecutionException {
		Buffer buffer = new Buffer();
		executorService.execute(new Producer(buffer, capacity));
		executorService.submit(new Consumer(buffer)).get();
		System.out.println("================");
	}

	private void runInfinite(ExecutorService executorService, int capacity) {
		while (true) {
			try {
				runProcess(executorService, capacity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void runLimited(ExecutorService executorService, int iteration, int capacity) {
		try {
			for (int i = 1; i <= iteration; i++) {
				runProcess(executorService, capacity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			executorService.shutdown();
		}
	}

	private Boolean isLimited(int iteration) {
		if (iteration > 0) {
			return true;
		} else {
			return false;
		}
	}

}
